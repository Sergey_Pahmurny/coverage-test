import { addNumbers, substractNumbers } from './app';

test('adds 1 + 2 to equal 3', () => {
  expect(addNumbers(1, 2)).toBe(3);
});

test('adds 10 - 5 to equal 5', () => {
  expect(substractNumbers(10, 5)).toBe(5);
});
