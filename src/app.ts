export const addNumbers = (a: number, b: number): number => {
  const result = a + b;
  console.log('A + B = ', result);
  return result;
};

export const substractNumbers = (a: number, b: number): number => {
  const result = a - b;
  console.log('A - B = ', result);
  return result;
};

export const multiplyNumbers = (a: number, b: number): number => {
  const result = a * b;
  console.log('A * B =', result);
  return result;
};

export default {
  addNumbers,
  substractNumbers,
  multiplyNumbers,
};
